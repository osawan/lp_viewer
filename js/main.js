$(document).ready(function(){

  var num_view = 1;//表示されているviewの枚数

  //マウスオーバーメニューを非表示に
  $(".mouseover_menu").css({"display":"none"});

  // ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼ drop時の処理 ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
  // 🌟画像表示の処理
  $("body").on("drop",".droparea",function(e){//.drop上でクリックボタンから手が離れたら
    e.preventDefault();//普段の挙動を抑止

    var index = $('.droparea').index(this);//何番目のdropareaにドロップされたか取得
    var files = e.originalEvent.dataTransfer.files;//Filesオブジェクトを代入
    var file = files[0];//読み込んだファイルの0番目を代入(1枚ずつしか読み込まない想定)
    var reader = new FileReader();//FileReaderオブジェクト作成

    reader.readAsDataURL(file);//↑で作ったreaderにfileの内容を入れる

    reader.onload = function (evt) {//↑が済んだら
      var file_src = evt.target.result;//画像のsrcを代入
      $(".droparea:eq("+ index +")").html('<img src="' + file_src + '" alt="" />');//画像表示
      $(".droparea:eq("+ index +")").css({"padding":"0" , "width":"360px"});//css調整(dropareaの矩形領域を作っていたpaddingを0にして、画像サイズぴったりに合わせる)
    };

    // 🌟新しいview追加の処理
    //もしdropされたviewに画像がはめ込まれていなかったら、空のviewを追加
    if(!$(this).children("img").length){//imgのlengthが存在しなかったら
      $("#jquery-ui-sortable").append('<div class="view"><div class="mouseover_menu"><div class="btn_close">×</div><p>◀︎ドラッグで入れ替え可能▶︎</p></div><div class="droparea">画像をドロップ</div></div>');
      num_view ++;
    }

  });

  $("body").on("dragover",".droparea",function(e){//dragoverの普段の挙動を抑止
    e.preventDefault();//普段の挙動を停止
  });


  // ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼ マウスオーバーメニューの処理 ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼
  $("body").on("mouseover",".view",function(){//マウスオーバー中のみ表示
    var index = $('.view').index(this);//何番目のviewにマウスオーバーされたか取得
    if(!$(this).dragover){//画像をドラッグ中は表示しない
      $(".mouseover_menu:eq("+ index +")").css({"display":"block"});
    };
  });

  $("body").on("mouseout",".view",function(){//マウスアウトしたら非表示
    $(".mouseover_menu").css({"display":"none"});
  });

  //×ボタンクリックでviewを削除
  $("body").on("click",".btn_close",function(){
    if(num_view > 1){//viewが2枚以上あったときだけ削除
    $(this).parent().parent().remove();//親の親要素を削除
    num_view --;
    }
  });


// ▼▼▼▼▼▼▼▼▼▼ドラッグ関連の処理▼▼▼▼▼▼▼▼▼▼
  $('#jquery-ui-sortable').sortable( {
          revert: true
  } );
  $('#jquery-ui-draggable').draggable( {
      connectToSortable: '#jquery-ui-sortable',
      helper: 'clone',
      revert: 'invalid',
  } );
  $('#jquery-ui-draggable-connectToSortable').disableSelection();

});
